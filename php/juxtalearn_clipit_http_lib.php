<?php
/**
 * Clip-It API HTTP client library for JuxtaLearn.
 *
 * @author Nick Freear, 13 March, 2 May 2014.
 * @copyright 2014 The Open University (IET).
 *
 * Chrome add-on:  Postman API client;
 * Chrome add-on:  XML Viewer;
 * Twitter app.:   Twitter caster;
 */
require_once 'http.php';
require_once 'juxtalearn_clipit_model.php';


class JuxtaLearn_ClipIt_HTTP_Lib extends JuxtaLearn_ClipIt_Model {

  const LOC_DOMAIN = JuxtaLearn_ClipIt_Client::LOC_DOMAIN;
  const CLIPIT_TIMEOUT = 1000000;
  const CLIPIT_API = '/services/api/rest/json?method=clipit.%s&';
  const DEF_BASE_URL = 'JXL_CLIPIT_BASE_URL';

  // ClipIt API token.
  private $auth_token;
  protected $messages = array();
  protected $request_count = 0;


  public function __construct() {
    if (!defined( self::DEF_BASE_URL )) {
      $this->error( 'Error. Missing PHP define(): '. self::DEF_BASE_URL );
      //throw new Exception(..)
    }
    $this->debug(array( 'clipit_url' => $this->get_option( 'jxl_clipit_base_url' )));

    $this->add_action( 'admin_notices', 'admin_notices' );
    $this->add_action( 'admin_init', 'ajax_authenticate' );

    $this->add_action( 'current_screen', 'admin_screen_debug' );
    $this->add_action( 'wp_ajax_clipit_test', 'clipit_api_test' );
  }


  /** WP action to display error/ debug/ info messages.
  */
  public function admin_notices() {
    foreach ($this->get_messages() as $msg):
      if (!is_string($msg['msg'])) {
        continue;
      }
      $classes = $msg['type'] . ('debug' == $msg['type'] ? ' warn update-nag': ' updated');
        ?>
    <div class="message clipit-msg <?php echo $classes ?>"><p><?php echo $msg['msg'] ?></div>
    <?php
    endforeach;
  }


  public function ajax_authenticate() {
    $regex = '@admin-ajax.php.+action=clipit_@';
    if (preg_match($regex, $_SERVER['REQUEST_URI']) && !is_user_logged_in()) {
      die( "Warning, not logged in" );
    }
    @header( 'Content-Type: text/plain' );
  }

  /** TEST.
  * wordpress/wp-admin/admin-ajax.php?action=clipit_test&method=site.api_list
  */
  public function clipit_api_test() {
    $this->ajax_authenticate();

    $api_method = isset($_GET['method']) ? $_GET['method'] : NULL;
    $ids = $this->_get( 'id' );
    $input = array(
      'id_array' => is_array( $ids ) ? $ids : NULL,
      'id' =>  is_numeric( $ids ) ? $ids : NULL,
    );
    foreach ($_GET as $key => $value) {
      if (preg_match('/(id|method|token)/', $key)) continue;
      $input[ $key ] = $value;
    }
    $resp = $this->api_request( $api_method, $input );
    $resp->obj->http_code = $resp->http_code;
    $resp->obj->url = $resp->url;
    $this->debug_request_count();
    echo "$resp->http_method $resp->url \nHTTP status: $resp->http_code".PHP_EOL;
    if ($resp->success) {
      print_r( $resp->obj );
      if ($this->_get('debug')) {
        print_r( $this->get_messages() );
      }
    } else {
      echo 'ERROR: '. $resp->curl_error;
    }
    die();
  }


  /** Main method to make a request to the ClipIt API. Handles authentication.
  * @param string $api_method  Examples 'site.get_token', 'quiz.create'
  * @param array  $input
  * @return object Response, with the result in $resp->obj->result. Also, $resp->success.
  */
  protected function api_request( $api_method = NULL, $input = array() ) {
    $api_method = $api_method ? $api_method : 'site.api_list';
    if (!$this->get_token()) {
      $resp = $this->do_request( 'site.get_token', array(
        'login'   => constant( 'JXL_CLIPIT_LOGIN' ),
        'password'=> constant( 'JXL_CLIPIT_PASSWORD' ),
        'timeout' => self::CLIPIT_TIMEOUT,
      ));

      if ($resp->success) {
        $this->auth_token = $resp->obj->result;
      }
    }
    if ('site.get_token' == $api_method) {
      return $resp;
    }
    return $this->do_request( $api_method, $input );
  }


  private function do_request( $api_method, $input ) {
    $this->request_count++;

    $is_get = preg_match( '/\.(get_|api_list|list_prop)/', $api_method );

    $this->debug( 'API request: '. $api_method );

    if ($this->get_token()) {
      $input[ 'auth_token' ] = $this->get_token();
    }

    $url = constant( self::DEF_BASE_URL ) . sprintf( self::CLIPIT_API, $api_method );
    //WAS: $url = sprintf(constant( 'JXL_CLIPIT_API_URL' ), 'json')
    //    .'?method=clipit.'. $api_method .'&';
    $payload = NULL;
    if ($is_get) {
      $url .= http_build_query( $input );
    } else {
      //$payload = http_build_query( $input );
      $payload = $input;
    }

    $http = new Http();
    $resp = $http->request( $url, $spoof = FALSE, array(
      'method' => $is_get ? 'GET' : 'POST',
      'ua' => 'Tricky Topic tool (WP/PHP/cURL) (+http://juxtalearn.net)',
      'data' => $payload,
    ));
    $resp->input = $payload;

    if ($resp->success) {
      $resp->obj = json_decode($resp->data);
      $resp->data = $resp->obj ? -1 : $resp->data;

      if (0 === $resp->obj->status) {
        $this->debug( "ClipIt API: OK, $resp->http_code: $url" );

        $resp->clipit_id = is_int($resp->obj->result) ? $resp->obj->result : NULL;
      } else {
        $resp->success = FALSE;
        $resp->curl_errno = $resp->obj->status;
        $resp->curl_error = $resp->obj->message;

        $resp->error_source = 'clipit';

        $this->error( 'ClipIt API: error '.
            $resp->curl_errno .': '. $resp->curl_error .' | '. $url );
      }
    }
    $resp->url = $url;
    $resp->http_method = $is_get ? 'GET' : 'POST';

    $this->file_logger( $resp );
    return $resp;
  }


  /** Debug headers.. */
  public function admin_screen_debug( $screen ) {
    $post_id = $this->_get( 'post' );
    $action = $this->_get( 'action' );
    $page = $this->_get( 'page' );
    $p_quiz_id = $this->_get( 'id' );

    if ($screen->base == 'post') {
      $clipit_id = $this->post_get_clipit_id( $post_id );
      $this->debug(array( __FUNCTION__, 'post_id' => $post_id, 'clipit_id' => $clipit_id, 'type' => $screen->post_type ));
    }
    elseif ($page == 'slickquiz-edit') {
      $quiz = $this->quiz_get_scaffold( $p_quiz_id );
      $clipit_id = $this->quiz_get_clipit_id( $quiz );
      $this->debug(array( __FUNCTION__, 'quiz_id' => $p_quiz_id, 'clipit_id' => $clipit_id, 'tt_id' => $quiz->tricky_topic_id ));
    } else {
      $this->debug(array( __FUNCTION__, 'other' ));
    }
  }


  /** Basic file logger.
  */
  protected function file_logger( $data ) {
    $bytes = -1;
    if ($this->get_option( 'jxl_clipit_file_log' )) {  #Was: 'juxtalearn_clipit_client_file_log'
      $path = $this->get_option( 'jxl_clipit_file_log_path',
	      '/var/www/logs/{http_host}-clipit-client-{date+Y-m-d.H}.log' );
      $path = strtr( $path, array( '{http_host}' => $_SERVER[ 'HTTP_HOST' ] ));
      if (preg_match( '@\{date ?\+([^\}]+)\}@', $path, $matches)) {
        $path = str_replace( $matches[ 0 ], date($matches[ 1 ]), $path );
      }
      $bytes = file_put_contents( $path, json_encode( array(
          date( 'c' ), $_SERVER[ 'REQUEST_URI' ], $data ) ). "\n\n", FILE_APPEND );
    }
    $this->debug( __FUNCTION__ .'; bytes='. $bytes );
    return $data;
  }

  /** Utilities.
  */
  protected function get_token() {
    return $this->auth_token;
  }

  protected function add_action( $hook, $function, $priority = 10, $accepted_args = 1 ) {
    add_action( $hook, array( &$this, $function ), $priority, $accepted_args );
  }

  protected function error( $text ) {
    return $this->message( $text, 'error' );
  }
  protected function debug( $text ) {
    return $this->message( $text, 'debug' );
  }
  protected function debug_request_count() {
    return $this->debug( 'ClipIt API, request count: '. $this->request_count );
  }

  protected function message( $text, $type = 'ok' ) {
    $message_r = array( 'type' => $type, 'msg' => $text );
    $this->messages[] = $message_r;
    @header('X-Jxl-Clipit-Msg-'.
        sprintf('%02d', count($this->messages)) .': '. json_encode( $message_r ));
  }
  protected function get_messages() {
    return $this->messages;
  }

  protected function _get( $key, $default = NULL ) {
    return isset($_GET[$key]) ? $_GET[$key] : $default;
  }

}
